Program output:

The binary representation of 1 is: 1
The binary representation of 17 is: 10001
The binary representation of 33 is: 100001
The binary representation of 35 is: 100011
The binary representation of 65 is: 1000001
The binary representation of 1000 is: 1111101000
The binary representation of 10000 is: 10011100010000
The binary representation of 11111111 is: 101010011000101011000111

The decimal representation of 0 is: 0
The decimal representation of 1 is: 1
The decimal representation of 10 is: 2
The decimal representation of 101 is: 5
The decimal representation of 111 is: 7
The decimal representation of 1010 is: 10
The decimal representation of 111111 is: 63
The decimal representation of 10000000 is: 128