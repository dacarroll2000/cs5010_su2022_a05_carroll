#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
using namespace std;

void bin2dec(int bin, int &dec);
void dec2bin(int num);

//functions for easy printing
void Printbin2dec (int bin, int &dec);
void Printdec2bin (int num);

int main() {
    vector<int> bin = {0,1,10,101,111,1010, 111111, 10000000};
    int dec = 0;
    vector<int> num = {1,17,33,35,65,1000,10000,11111111};

    //for_each was convenient because only 1 variable is fed to Printdec2bin at a time
    for_each (num.begin(),num.end(),Printdec2bin);
    cout << endl;

    //a normal for loop was used because 2 variables were fed to Printbin2dec at a time
    for (int i = 0; i < bin.size(); i++) {
        Printbin2dec(bin.at(i), dec);
    }

    return 0;
}

void bin2dec(int bin, int &dec) {
    //digits is used to count the number of digits in bin
    //binCopy is a copy of bin used while counting the digits of bin and while modifying bin for recursion
    int digits = 1;
    int binCopy = bin;

    //this is a catch for when bin is 0. bin == 0 is only ever true when a bin of 0 is given to Printbin2dec
    if (bin == 0) {
        dec = 0;
        return;
    }

    // This counts the digits in bin. 101 would have 3 digits, 1000 would have 4 digits, etc.
    while (binCopy >= 10) {
        binCopy /= 10;
        digits += 1;
    }

    //2^(digits-1) is added to dec. 2^(digits-1) is the value of bin if only the leftmost number was 1.
    //example: if bin = 101 then digits = 3. 2^(3-1) = 4. Thus, 4 is added to dec
    //the remaining value of bin will be added to dec in subsequent calls
    dec += pow(2,digits-1);

    //this is used to remove the rightmost digit from num.
    //binCopy is multiplies by 10 until it is the same order of magnitude as bin. binCopy will always be 1 when this process begins.
    //example: bin = 101 and binCopy enters the loop with a value of 1. binCopy will exit the loop with a value of 100.
    for (int i = 0; i < digits-1; i++) {
        binCopy *= 10;
    }

    //binCopy will be subtracted from bin.
    //example: bin = 101 and binCopy is currently 100. 101-100 = 1, so bin is now 1
    //example: bin = 111 and binCopy is currently 100. 111-100 = 11, so bin is now 11
    bin -= binCopy;

    //this checks if bin is above 0
    if (bin > 0) {
        //another layer of recursion
        bin2dec(bin,dec);
    }
}

void dec2bin(int num) {
    if (num > 1) {
        //prints (num/2)%2
        dec2bin(num/2);
    }

    //the end of the recursive line prints first and the beginning of the recursive line prints last
    cout << num%2;
}


void Printbin2dec (int bin, int &dec) {
    dec = 0;
    bin2dec(bin, dec);
    cout << "The decimal representation of " << bin << " is: ";
    cout << dec << endl;
}
void Printdec2bin (int num) {
    cout << "The binary representation of " << num << " is: ";
    dec2bin(num);
    cout << endl;
}